function click1() {
    let f1 = document.getElementById("value");
    let f2 = document.getElementById("col");
    let r = document.getElementById("result");
    if ((/^[1-9][0-9]*$/.test(f1.value)) && (/^[1-9][0-9]*$/.test(f2.value))) {
        r.innerHTML = "Итого: " + f1.value * f2.value;
    } else {
        r.innerHTML = "Итого: введите правильное число";
    }
}
window.addEventListener("DOMContentLoaded", function () {
    let a = document.getElementById("my-button");
    a.addEventListener("click", click1);
});
